module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        /**
         * Validate the source code files to ensure they
         * follow our coding convention and
         * dont contain any common errors.
         */
        jshint: {
            all: [
                "Gruntfile.js",
                "app.js",
                "app/**/*.js",
                "app-test/specs/*.js",
                "!touch/**/*.js"
            ],
            options: {
                trailing: true,
                eqeqeq: true
            }
        },
        /**
         * Setups Jasmine and runs them using PhantomJS headlessly.
         */
        sencha_touch_jasmine: {
            options: {
                specs: ["app-test/specs/**/*.js"],
                extFramework: "touch",
                extLoaderPaths   : {
                    "TutsApp" : "app"
                },
                extBootstrapFile: 'app-test.js',
                extAppName: 'TutsApp'
            },
            app: {
                extLoaderPaths   : {
                    "TutsApp" : "app"
                }
            }
        },
        /**
        * setup watch
        */
        watch: {
            // create new task
            frontend : {
                // point which files should be watched
                files: [
                    'app/**/*.js',
                    'app-test/**/*.js',
                    'app.js'
                ],
                // define what tasks should be run
                // if watch trigger any changes
                tasks: ['jshint', 'sencha_touch_jasmine:app'],
                options: {
		    		livereload: true
                }
            }
        },
	pkg: grunt.file.readJSON('package.json'),
	uglify: {
	    options: {
		banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
	    },
	    build: {
		src: 'src/<%= pkg.name %>.js',
		dest: 'build/<%= pkg.name %>.min.js'
	    }
	},
	less: {
	    development: {
		options: {
		    paths: ["assets/css"]
		},
		files: {
		    "build/style.css": "src/style.less"
		}
	    }
	},
	jade: {
	    compile: {
		options: {
		    data: {
			debug: false
		    }
		},
		files: {
		    "build/index.html": ["src/index.jade"]
		}
	    }
	}
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-jade');
 
    grunt.loadNpmTasks("grunt-contrib-jshint");
    grunt.loadNpmTasks("grunt-sencha-jasmine");
    grunt.loadNpmTasks("grunt-contrib-watch");
 
    grunt.registerTask("default", [
        "jshint"
    ]);
    // Default task(s).
    grunt.registerTask('default', ['uglify', 'less', 'jade']);
    grunt.registerTask("test", ["watch:frontend"]);

};
