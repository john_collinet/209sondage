$(document).ready(function() {
	$("#result").hide();
	$("#error").hide();
	$("#error_empt").hide("slow");
	$("#error_numb").hide("slow");
	$("#error_sup").hide("slow");
});

function catch_calc() {
	var t_pop = document.getElementById('i1').value;
	var t_ech = document.getElementById('i2').value;
	var p_vot = document.getElementById('i3').value;

	$("#error").hide("slow");
	$("#error_sup").hide("slow");
	$("#error_empt").hide("slow");
	$("#error_numb").hide("slow");
	$("#result").hide("slow");
	if (t_pop == "" || t_ech == "" || p_vot == "")
		{
			$("#error").show("slow");
			$("#error_empt").show("slow");
			return ;
		}
	if (isNaN(t_pop) || isNaN(t_ech) || isNaN(p_vot))
		{
			$("#error").show("slow");
			$("#error_numb").show("slow");
			return ;
		}

	t_pop = parseFloat(t_pop);
	t_ech = parseFloat(t_ech);

	if (t_ech > t_pop)
		{
			$("#error").show("slow");
			$("#error_sup").show("slow");
			return ;
		}

	p_vot = parseFloat(p_vot);

	if (p_vot > 100 || p_vot < 0 || t_pop < 1 || t_ech < 1)
		{
			$("#error").show("slow");
			$("#error_numb").show("slow");
			return ;
		}

	document.getElementById('r1').innerHTML = t_pop;
	document.getElementById('r2').innerHTML = t_ech;
	document.getElementById('r3').innerHTML = p_vot;

    var variance = parseFloat(((((p_vot / 100.0) * (1.0 - (p_vot / 100.0))) / t_ech) * ((t_pop - t_ech) / (t_pop - 1.0))));
	document.getElementById('r4').innerHTML = variance.toFixed(6);

	var p1 = p_vot - (((2 * 1.96) * Math.sqrt(variance)) / 2 * 100);
    if (p1 < 0.0)
	p1 = 0.0;
	document.getElementById('p1').innerHTML = p1.toFixed(2);
	var p2 = p_vot + (((2 * 1.96) * Math.sqrt(variance)) / 2 * 100);
    if (p2 > 100.0)
	p2 = 100.0;
	document.getElementById('p2').innerHTML = p2.toFixed(2);

	var p3 = p_vot - (((2 * 2.58) * Math.sqrt(variance)) / 2 * 100);
    if (p3 < 0.0)
	p3 = 0.0;
	document.getElementById('p3').innerHTML = p3.toFixed(2);
	var p4 = p_vot + (((2 * 2.58) * Math.sqrt(variance)) / 2 * 100);
    if (p4 > 100.0)
	p4 = 100.0;
	document.getElementById('p4').innerHTML = p4.toFixed(2);

	$("#result").show("slow");
	$("#error").hide("slow");
	$("#usage").hide("slow");
}
